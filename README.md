# HTTP Router deployer

This project holds the CI pipelines to deploy the [HTTP Router service](https://gitlab.com/gitlab-org/cells/http-router).

The application code is maintained at [`gitlab-org/cells/http-router`](https://gitlab.com/gitlab-org/cells/http-router).

## Workflow

1. MR is created, review and merged in [`gitlab-org/cells/http-router`](https://gitlab.com/gitlab-org/cells/http-router)
2. CI job in [`gitlab-org/cells/http-router`](https://gitlab.com/gitlab-org/cells/http-router) triggers pipeline in [`gitlab-com/gl-infra/cells/http-router-deployer`](https://gitlab.com/gitlab-com/gl-infra/cells/http-router-deployer) on the `ops` instance
3. `http-router-deployer` pipeline deploys the `main` branch of `http-router` over a phased rollout per environment, with the following order: `gstg`, `gprd`
