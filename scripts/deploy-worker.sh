#!/usr/bin/env bash

# Deploy Worker
#
# Purpose:
# This script automates the deployment process for the HTTP Router.
# It supports different deployment strategies and provides options for monitoring the deployment.
#
# Usage:
#   ./deploy-worker.sh [options]
#
# Environment Variables:
#   DEPLOYMENT_STRATEGY       Sets the deployment strategy (default: "rollout")
#                             Supported values: "rollout", "expedited"
#   DEPLOYMENT_STEP_WAIT      Time to wait between deployment steps in minutes (default: 3)
#   CI_ENVIRONMENT_SLUG       The environment to deploy to (required)
#   GITLAB_PROXY_HOST         The host for the GitLab proxy (required)
#   SENTRY_DSN                The DSN for the Sentry project (required)
#   OVERRIDE_LAST_PERCENTAGE  If set to true script will not fail if ROLLOUT_PERCENTAGES sequence ends with value different than 100%
#   WORKER_NAME               Name of the target Cloudflare Worker, same as name present in `wrangler.toml` (required).
#   GITLAB_API_TOKEN          API Token to use for triggering the rollback. Required for auto-rollback functionality.
#
# Description:
# This script performs the following actions:
# 1. Uploads a new version of the worker to Cloudflare
# 2. Retrieves the IDs of the new and current versions
# 3. Deploys the new version using the specified strategy:
#    - "rollout": Gradually increases traffic to the new version
#    - "expedited": Immediately routes 100% of traffic to the new version
# 4. Validate that the site is working by making a request to the new version.
#
# The script uses Cloudflare's wrangler CLI tool for deployments and
# relies on various CI/CD environment variables from GitLab.

set -o pipefail
set -e

DEPLOYMENT_STRATEGY=${DEPLOYMENT_STRATEGY:-"rollout"}
DEPLOYMENT_STEP_WAIT=${DEPLOYMENT_STEP_WAIT:-3}
ROLLOUT_PERCENTAGES="5 25 50 75 100"

# validate that WORKER_NAME is defined.
if [ -z "$WORKER_NAME" ]; then
    echo "ERROR: WORKER_NAME environment variable is not set"
    echo "Please set the WORKER_NAME environment variable before running this script"
    exit 1
fi

# validate DEPLOYMENT_STRATEGY is one of "rollout" or "expedited"
if [[ "$DEPLOYMENT_STRATEGY" != "rollout" && "$DEPLOYMENT_STRATEGY" != "expedited" ]]; then
    echo "Error: Invalid DEPLOYMENT_STRATEGY. Supported values are 'rollout' or 'expedited'."
    exit 1
fi

# Validate ROLLOUT_PERCENTAGES
IFS=' ' read -ra percentages <<<"$ROLLOUT_PERCENTAGES"
last_percentage=${percentages[-1]}

if [ -z "$OVERRIDE_LAST_PERCENTAGE" ]; then
    if [ "$last_percentage" != "100" ]; then
        echo "Error: The last value in ROLLOUT_PERCENTAGES must be 100"
        exit 1
    fi
fi

for percentage in "${percentages[@]}"; do
    if ! [[ "$percentage" =~ ^[0-9]+$ ]] || [ "$percentage" -lt 0 ] || [ "$percentage" -gt 100 ]; then
        echo "Error: Invalid percentage value in ROLLOUT_PERCENTAGES: $percentage"
        echo "All values must be non-negative integers between 0 and 100"
        exit 1
    fi
done

DEPLOY_MESSAGE="Deployed with JOB ID: $CI_JOB_ID"

upload_new_version() {
    ## upload_new_version uploads new version of your HTTP Router that is not deployed immediately.
    ## https://developers.cloudflare.com/workers/wrangler/commands/#upload
    wrangler versions upload --tag "$CI_COMMIT_SHORT_SHA" --message="$DEPLOY_MESSAGE" --env="${CI_ENVIRONMENT_SLUG}" --var="GITLAB_PROXY_HOST:${GITLAB_PROXY_HOST}" --var="SENTRY_DSN:${SENTRY_DSN}" --var="GITLAB_ENV=${CI_ENVIRONMENT_SLUG}"
}

set_version_ids() {
    ## set_version_ids retrieves and sets the IDs for the new (recently uploaded) and old (currently deployed) versions of the worker.
    ## It exports these IDs as environment variables to be used by the deploy_versions function.
    ## https://developers.cloudflare.com/workers/wrangler/commands/#list-6
    NEW_VERSION_ID=$(wrangler versions list --env="${CI_ENVIRONMENT_SLUG}" --json | jq -r --arg DEPLOY_MESSAGE "$DEPLOY_MESSAGE" '.[] | select(.annotations["workers/message"] == $DEPLOY_MESSAGE ) | .id')
    export NEW_VERSION_ID

    OLD_VERSION_ID=$(wrangler deployments status --env="${CI_ENVIRONMENT_SLUG}" --json | jq -r '
        .versions | 
        sort_by(.percentage) | 
        reverse | 
        .[0].version_id
    ')
    export OLD_VERSION_ID
}

deploy_versions() {
    ## deploy_versions deploys two versions of the worker with specified traffic percentages.
    ## It takes a single argument: the percentage of traffic for the new version.
    ## The function validates the input, calculates the old version's percentage,
    ## and uses wrangler to deploy both versions with their respective traffic splits.
    ## https://developers.cloudflare.com/workers/wrangler/commands/#deploy-2
    local new_version_percent="$1"

    # Validate that new_version_percent is an integer
    if ! [[ "$new_version_percent" =~ ^[0-9]+$ ]]; then
        echo "Error: New version percentage must be an integer"
        return 1
    fi

    # Convert to integer (removes leading zeros)
    new_version_percent=$((10#$new_version_percent))

    # Ensure the percentage is valid
    if [ "$new_version_percent" -lt 0 ] || [ "$new_version_percent" -gt 100 ]; then
        echo "Error: New version percentage must be between 0 and 100"
        return 1
    fi

    local old_version_percent=$((100 - new_version_percent))

    echo "Deploying new version ($NEW_VERSION_ID) at $new_version_percent% and old version ($OLD_VERSION_ID) at $old_version_percent%"

    # shellcheck disable=SC2086
    wrangler versions deploy $NEW_VERSION_ID@$new_version_percent $OLD_VERSION_ID@$old_version_percent -y --env="${CI_ENVIRONMENT_SLUG}"
}

get_rollback_job_id() {
  ## get_rollback_job_id finds the job ID for the rollback job in the current pipeline.
  ## It uses the GitLab API to fetch all manual jobs and filters for the rollback job
  ## that matches the current environment (from $CI_ENVIRONMENT_NAME).
  ## 
  ## Returns:
  ##   The job ID for the rollback job, or exits with error status if not found
  local job_name="rollback-worker-${CI_ENVIRONMENT_NAME}"
  local response
  local job_id
  
  # Make the API call with error handling
  if ! response=$(curl --silent --fail --header "PRIVATE-TOKEN:$GITLAB_API_TOKEN" \
    "$CI_API_V4_URL/projects/$CI_PROJECT_ID/pipelines/$CI_PIPELINE_ID/jobs?scope=manual" 2>&1); then
    echo "Error: Failed to fetch jobs from GitLab API: $response" >&2
    return 1
  fi
  
  # Check if response is empty
  if [ -z "$response" ]; then
    echo "Error: Empty response from GitLab API" >&2
    return 1
  fi
  
  # Extract job ID using jq
  job_id=$(echo "$response" | jq -r --arg job_name "$job_name" '.[] | select(.name == $job_name) | .id')
  
  # Check if job ID was found
  if [ -z "$job_id" ] || [ "$job_id" = "null" ]; then
    echo "Error: No job with name '$job_name' found" >&2
    return 1
  fi
  
  echo "$job_id"
  return 0
}

rollback_deployment() {
  ## rollback_deployment triggers the rollback job for the current environment.
  ## This function is called when validation fails during deployment.
  ## It uses get_rollback_job_id to find the appropriate job and then
  ## triggers it using the GitLab API's play endpoint.
  ##
  ## Returns:
  ##   0 on success, 1 on failure
  local job_id
  local play_response
  
  echo "Starting rollback for environment: $CI_ENVIRONMENT_NAME"
  
  # Get the job ID for the rollback job
  if ! job_id=$(get_rollback_job_id); then
    echo "Failed to get job ID for $CI_ENVIRONMENT_NAME environment" >&2
    return 1
  fi
  
  echo "Found rollback job ID: $job_id for environment: $CI_ENVIRONMENT_NAME"
  
  # Trigger the job using the play endpoint
  if ! play_response=$(curl -X POST --silent --fail --header "PRIVATE-TOKEN:$GITLAB_API_TOKEN" \
    "$CI_API_V4_URL/projects/$CI_PROJECT_ID/jobs/$job_id/play" 2>&1); then
    echo "Error: Failed to trigger rollback job: $play_response" >&2
    return 1
  fi
  
  echo "Successfully triggered rollback job for environment: $CI_ENVIRONMENT_NAME"
  echo "Job details: $(echo "$play_response" | jq -r '.web_url // "No URL available"')"
    return 0
}

validate_deployment() {
    ## validate_deployment sends a request to verify that the deployed version is responding correctly.
    ## It uses the version_id to set the header in the format 'Cloudflare-Workers-Version-Overrides: $WORKER_NAME="$version_id"'
    ## The version override will only be applied if the version is in the current active dployment.
    ## Please refer to Cloudflare docs to learn more: https://developers.cloudflare.com/workers/configuration/versions-and-deployments/gradual-deployments/#version-overrides
    ## The function returns success (0) if the request succeeds, and failure (1) otherwise.
    local version_id="$1"
    # Using the /-/http_router/version endpoint, as it can be connected directly without being logged in or redirected to the /users/sign_id page
    local validation_url="https://${GITLAB_PROXY_HOST}/-/http_router/version"
    local response_file="response.txt"

    echo "Validating deployment of version $version_id..."

    # Execute curl command directly, not in a subshell
    # This allows it to correctly write to the response_file
    curl -s -o "$response_file" -w "%{http_code}" \
        -H "Cloudflare-Workers-Version-Overrides: ${WORKER_NAME}=\"${version_id}\"" \
        "$validation_url" >status.txt

    # Read the status code from the file
    local status_code
    status_code=$(cat status.txt)
    rm -f status.txt

    # Check if status code is not 200 - fail early
    if [ "$status_code" -ne 200 ]; then
        echo "ERROR: Validation failed: Version $version_id responded with status $status_code"
        echo "Response content:"
        echo "================================================================================"
        cat "\\n$response_file\\n"
        echo "================================================================================"
        echo "======================================================================================"
        echo "DEPLOYMENT FAILED: The new version is not responding correctly and may have introduced a regression."
        echo "Please check the latest commit and fix any issues before redeploying."
        echo "======================================================================================"
        rm -f "$response_file"
        return 1
    fi

    # Extract the ID from the response directly checking the command result
    local returned_id
    if ! returned_id=$(jq -r '.id' "$response_file" 2>/dev/null) ||
        [ "$returned_id" = "null" ] ||
        [ -z "$returned_id" ]; then
        echo "ERROR: Failed to extract ID from response or ID is null"
        echo "Response content:"
        cat "$response_file"
        rm -f "$response_file"
        return 1
    fi

    # Check if IDs don't match - fail early
    if [ "$returned_id" != "$version_id" ]; then
        echo "ERROR: Validation failed: Expected version ID $version_id but got $returned_id"
        echo "Response content:"
        cat "$response_file"
        rm -f "$response_file"
        return 1
    fi

    # If we got here, validation succeeded
    echo "Validation successful: Version $version_id correctly responded with matching ID"
    rm -f "$response_file"
    return 0
}

deploy_with_strategy() {
    ## deploy_with_strategy executes the deployment based on the specified strategy.
    ## It supports two strategies:
    ##   1. 'expedited': Deploys 100% of traffic to the new version immediately.
    ##   2. 'rollout': Gradually increases traffic to the new version based on ROLLOUT_PERCENTAGES.
    ## The function uses deploy_versions to handle the actual deployment for each step.
    ## For the rollout strategy, it includes a waiting period (DEPLOYMENT_STEP_WAIT) between each step.
    ## If validation fails at any step, it will trigger a rollback before exiting with failure.
    ## Rollback is only supported when running in GitLab CI environment.
    
    # Helper function for handling validation failure
    handle_validation_failure() {
        echo "Validation failed."
        
        # Check if we're running in GitLab CI
        if [ "${GITLAB_CI}" = "true" ]; then
            echo "Initiating rollback..."
            rollback_deployment
        else
            echo "Rollback not supported for deploys not done from CI jobs."
        fi
        
        exit 1
    }

    case "$DEPLOYMENT_STRATEGY" in
    expedited)
        echo "Deploying with expedited strategy (100% rollout)"
        deploy_versions 100
        if ! validate_deployment "$NEW_VERSION_ID"; then
            handle_validation_failure
        fi
        ;;
    rollout)
        echo "Deploying with rollout strategy"
        for percentage in $ROLLOUT_PERCENTAGES; do
            echo "Rolling out $percentage%"
            deploy_versions "$percentage"
            if ! validate_deployment "$NEW_VERSION_ID"; then
                handle_validation_failure
            fi
            if [ -z "$OVERRIDE_LAST_PERCENTAGE" ]; then
                if [ "$percentage" -lt 100 ]; then
                    echo "Waiting for $DEPLOYMENT_STEP_WAIT minutes before next step"
                    echo "=========================================================="
                    echo "If you have noticed any regression, cancel the job and trigger the manual rollback job."
                    echo "=========================================================="
                    sleep $((DEPLOYMENT_STEP_WAIT * 60))
                fi
            fi
        done
        echo "Rollout complete."
        ;;
    *)
        echo "Error: Unknown deployment strategy '$DEPLOYMENT_STRATEGY'"
        exit 1
        ;;
    esac
}

# Main execution
upload_new_version
set_version_ids
deploy_with_strategy
